package mtconfig

import (
	"fmt"

	"github.com/BurntSushi/toml"
)

const COMMON = "common"

type Common struct {
	LogLevel string `toml:"log_level"`
	LogPath  string `toml:"log_path"`
}

func LoadConfig(path string, name string, config interface{}) error {

	var m map[string]toml.Primitive
	if _, err := toml.DecodeFile(path, &m); err != nil {
		return err
	}

	c, ok := m[name]
	if !ok {
		return fmt.Errorf("config not found section: %s", name)
	}

	return toml.PrimitiveDecode(c, config)
}

func LoadCommonConfig(path string, c interface{}) error {

	err := LoadConfig(path, COMMON, c)
	if err != nil {
		return err
	}

	return nil
}
