package mtdealer

// Config ..
type Config struct {
	ServerAddrs []string `toml:"mt4_server_addr"`
	Account     int      `toml:"mt4_account"`
	Password    string   `toml:"mt4_password"`
	WorkerCount int      `toml:"worker_count"`
	Timezone    int      `toml:"mt4_timezone"`
}
