package mtdealer

import (
	"errors"
	"fmt"
	"git.what.codes/go/mtsdk/mtlog"
	"git.what.codes/go/mtsdk/mtmanapi"
	"time"
)

// Dealer ...
type Dealer struct {
	manager     *DealerManager
	serverAddrs []string
	account     int
	password    string
	stop        chan bool
	stopped     bool
	nworker     int
	mtapi       mtmanapi.CManagerInterface
}

// NewDealer ...
func NewDealer(manager *DealerManager, serverAddrs []string, account int,
	password string, nworker int) *Dealer {
	return &Dealer{
		manager:     manager,
		serverAddrs: serverAddrs,
		account:     account,
		password:    password,
		stop:        make(chan bool, 1),
		nworker:     nworker,
		stopped:     false,
	}
}

// Stop ..
func (d *Dealer) Stop() {
	d.stopped = true
	close(d.stop)
}

// Run ..
func (d *Dealer) Run() {
	for !d.stopped {
		if d.mtapi == nil {
			mtlog.Infof("Dealer %d connecting to %+v", d.nworker, d.serverAddrs)
			mtapi, err := manAPISelectAuth(d.serverAddrs, d.account, d.password)
			//mtapi, err := manApiAuth(d.serverAddr, d.account, d.password)

			if err != nil {
				mtlog.Infof("Dealer %d connect error %s", d.nworker, err)
				select {
				case <-time.After(3 * time.Second):
				case <-d.stop:
				}
				continue
			}

			mtlog.Infof("Dealer %d connectted", d.nworker)
			d.mtapi = mtapi
		}

		var err error
		select {
		case task := <-d.manager.taskChan:
			ret := d.runTask(task)
			if ret == mtmanapi.RET_NO_CONNECT {
				err = errors.New("No Connection")
			}
		case <-time.After(3 * time.Second):
			ret := d.mtapi.Ping()
			if ret != mtmanapi.RET_OK {
				err = fmt.Errorf("Dealer %d ping return error: %d %s",
					d.nworker, ret, d.mtapi.ErrorDescription(ret))
			}
		}

		if err != nil {
			mtlog.Errorf("Dealer %d return error %s", d.nworker, err)
			d.destroyApi()
		}
	}

	d.destroyApi()
}

func (d *Dealer) runTask(task *dealerTask) int {
	if task.begin() != taskRunning {
		return mtmanapi.RET_OK
	}
	defer task.done()
	return task.taskFunc(d.mtapi)
}

func (d *Dealer) destroyApi() {
	if d.mtapi != nil {
		mtlog.Infof("destroying mtman api.")
		d.mtapi.Release()
		d.mtapi = nil
	}
}
