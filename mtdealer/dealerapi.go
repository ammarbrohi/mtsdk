package mtdealer

import (
	"errors"
	"time"

	"git.what.codes/go/mtsdk/mterr"
	"git.what.codes/go/mtsdk/mtmanapi"
)

type Assest struct {
	Login       int     `json:"login"`
	Balance     float64 `json:"balance"`
	Credit      float64 `json:"credit"`
	Margin      float64 `json:"margin"`
	FreeMargin  float64 `json:"free_margin"`
	MarginLevel float64 `json:"margin_level"`
	Equity      float64 `json:"equity"`
	Leverage    int     `json:"leverage"`
}

type TradeTrans struct {
	OrderBy     int
	Order       int
	Type        int
	Cmd         int
	Symbol      string
	Volume      int
	Price       float64
	Tp          float64
	Sl          float64
	Comment     string
	Expiration  int
	IEDeviation int
}

type UserInfo struct {
	Login int
	Name  string
	City  string
	Group string
}

type ChartInfo struct {
	Symbol   string // symbol
	Period   int    // period (PERIOD_*)
	Start    int    // start of chart block
	End      int    // end of chart block
	Timesign int    // timestamp of existing chart base
	Mode     int    // request mode
}

type RateInfo struct {
	CTM   int //RateTime
	Open  int
	High  int
	Low   int
	Close int
	Vol   float64
}

// TickInfo ...
type TickInfo struct {
	Symbol string  `json:"symbol"`
	Time   int     `json:"time"`
	Bid    float64 `json:"bid"`
	Ask    float64 `json:"ask"`
}

func (m *DealerManager) CreateAccount(login int, name string, password string,
	investorPwd string, group string, city string, email string, phone string,
) (retUser *UserInfo, retError error) {

	taskFunc := func(api mtmanapi.CManagerInterface) int {
		user := mtmanapi.NewUserRecord()
		user.SetLogin(login)
		user.SetName(name)
		user.SetPassword(password)
		user.SetEnable_change_password(1)
		user.SetEnable(1)
		user.SetPassword_investor(investorPwd)
		user.SetGroup(group)
		user.SetCity(city)
		user.SetLeverage(100)
		user.SetEmail(email)
		user.SetPhone(phone)

		ret := api.UserRecordNew(user)
		if ret != mtmanapi.RET_OK {
			retError = newMtErr(api, ret)
		}

		retUser = &UserInfo{
			Login: user.GetLogin(),
			Name:  user.GetName(),
			Group: user.GetGroup(),
			City:  user.GetCity(),
		}

		mtmanapi.DeleteUserRecord(user)
		return ret
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retError = err
	}

	return
}

func (m *DealerManager) CreateUserRecord(u *User) (retUser *UserInfo, retError error) {

	taskFunc := func(api mtmanapi.CManagerInterface) int {
		user := mtmanapi.NewUserRecord()
		user.SetLogin(u.Login)
		user.SetName(u.Name)
		user.SetCountry(u.Country)
		user.SetCity(u.City)
		user.SetAddress(u.Address)
		user.SetPhone(u.Phone)
		user.SetEmail(u.Email)
		user.SetGroup(u.Group)
		user.SetId(u.ID)
		user.SetLeverage(u.Leverage)
		user.SetPassword(u.Password)
		user.SetPassword_investor(u.PasswordInvestor)
		user.SetComment(u.Comment)
		user.SetUser_color(u.UserColor)
		user.SetAgent_account(u.AgentAccount)

		user.SetEnable(u.Enable)
		user.SetEnable_read_only(u.EnableReadOnly)
		user.SetEnable_change_password(u.EnableChangePassword)

		ret := api.UserRecordNew(user)
		if ret != mtmanapi.RET_OK {
			retError = newMtErr(api, ret)
		}

		retUser = &UserInfo{
			Login: user.GetLogin(),
			Name:  user.GetName(),
			Group: user.GetGroup(),
			City:  user.GetCity(),
		}

		mtmanapi.DeleteUserRecord(user)
		return ret
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retError = err
	}

	return
}

func (m *DealerManager) ModifyUser(login int, name, city, group string) (retOk bool, retError error) {

	taskFunc := func(api mtmanapi.CManagerInterface) int {

		total := 1
		user := api.UserRecordsRequest(&login, &total)
		if total <= 0 {
			retError = errors.New("User not found")
			return mtmanapi.RET_OK
		}

		user.SetName(name)
		user.SetGroup(group)
		user.SetCity(city)

		retmodify := api.UserRecordUpdate(user)

		if retmodify != mtmanapi.RET_OK {
			retError = newMtErr(api, retmodify)
		}

		retOk = retmodify == mtmanapi.RET_OK

		api.MemFree(user.Swigcptr())
		return retmodify
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retError = err
	}
	return
}

func (m *DealerManager) ModifyUserRecord(u *User) (retOk bool, retError error) {

	taskFunc := func(api mtmanapi.CManagerInterface) int {
		var (
			total = 1
			login = u.Login
			user  = api.UserRecordsRequest(&login, &total)
		)

		if total <= 0 {
			retError = errors.New("User not found")
			return mtmanapi.RET_OK
		}

		user.SetName(u.Name)
		user.SetCountry(u.Country)
		user.SetCity(u.City)
		user.SetAddress(u.Address)
		user.SetPhone(u.Phone)
		user.SetEmail(u.Email)
		user.SetGroup(u.Group)
		user.SetId(u.ID)
		user.SetLeverage(u.Leverage)
		user.SetComment(u.Comment)
		user.SetUser_color(u.UserColor)
		user.SetAgent_account(u.AgentAccount)

		user.SetEnable(u.Enable)
		user.SetEnable_read_only(u.EnableReadOnly)
		user.SetEnable_change_password(u.EnableChangePassword)

		retmodify := api.UserRecordUpdate(user)

		if retmodify != mtmanapi.RET_OK {
			retError = newMtErr(api, retmodify)
		}

		retOk = retmodify == mtmanapi.RET_OK

		api.MemFree(user.Swigcptr())
		return retmodify
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retError = err
	}
	return
}

func (m *DealerManager) ChangeGroup(login int, group string) (retUSer *UserInfo, retError error) {
	task := func(api mtmanapi.CManagerInterface) int {
		var total = 1
		userRecords := api.UserRecordsRequest(&login, &total)
		if total == 1 {
			userRecord := mtmanapi.UserRecordArray_getitem(userRecords, 0)
			userRecord.SetGroup(group)
			ret := api.UserRecordUpdate(userRecord)
			if ret != mtmanapi.RET_OK {
				retError = newMtErr(api, ret)
			}
			defer mtmanapi.DeleteUserRecord(userRecord)
		}

		api.MemFree(userRecords.Swigcptr())
		return total
	}

	if err := newTask(m, task, 5*time.Second).run(); err != nil {
		retError = err
	}
	return
}

func (m *DealerManager) CheckPassword(login int, password string,
) (retOk bool, retError error) {

	taskFunc := func(api mtmanapi.CManagerInterface) int {
		ret := api.UserPasswordCheck(login, password)
		if ret != mtmanapi.RET_OK {
			retError = newMtErr(api, ret)
		}

		retOk = ret == mtmanapi.RET_OK
		return ret
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retError = err
	}

	return
}

func (m *DealerManager) ResetPassword(login int, password string,
	change_investor int, clean_pubkey int) (retError error) {

	taskFunc := func(api mtmanapi.CManagerInterface) int {
		ret := api.UserPasswordSet(login, password, change_investor, clean_pubkey)
		if ret != mtmanapi.RET_OK {
			retError = newMtErr(api, ret)
		}

		return ret
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retError = err
	}

	return
}

func (m *DealerManager) GetAsset(login int) (retAsset *Assest, retError error) {

	taskFunc := func(api mtmanapi.CManagerInterface) int {
		total := 1
		user := api.UserRecordsRequest(&login, &total)
		if total <= 0 {
			retError = errors.New("User not found")
			return mtmanapi.RET_OK
		}

		marginLevel := mtmanapi.NewMarginLevel()
		ret := api.MarginLevelRequest(login, marginLevel)
		if ret == mtmanapi.RET_OK {
			retAsset = &Assest{
				Login:       login,
				Balance:     user.GetBalance(),
				Credit:      user.GetCredit(),
				Margin:      marginLevel.GetMargin(),
				MarginLevel: marginLevel.GetMargin_level(),
				FreeMargin:  marginLevel.GetMargin_free(),
				Leverage:    user.GetLeverage(),
				Equity:      marginLevel.GetEquity(),
			}
		} else {
			retError = newMtErr(api, ret)
		}

		api.MemFree(user.Swigcptr())
		return ret
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retError = err
	}

	return
}

func (m *DealerManager) TradeTransaction(trans *TradeTrans,
) (retTrans *TradeTrans, retError error) {

	taskFunc := func(api mtmanapi.CManagerInterface) int {
		t := mtmanapi.NewTradeTransInfo()

		t.SetOrderby(trans.OrderBy)
		t.SetOrder(trans.Order)
		t.SetXtype(byte(trans.Type))
		t.SetCmd(int16(trans.Cmd))
		t.SetSymbol(trans.Symbol)
		t.SetVolume(trans.Volume)
		t.SetPrice(trans.Price)
		t.SetTp(trans.Tp)
		t.SetSl(trans.Sl)
		t.SetComment(trans.Comment)
		t.SetExpiration(trans.Expiration)
		t.SetIe_deviation(trans.IEDeviation)

		ret := api.TradeTransaction(t)

		if ret != mtmanapi.RET_OK {
			retError = newMtErr(api, ret)
		} else {
			retTrans = &TradeTrans{}
			*retTrans = *trans
			retTrans.Volume = t.GetVolume()
			retTrans.Order = t.GetOrder()
			retTrans.Price = t.GetPrice()
			retTrans.Comment = t.GetComment()
		}

		mtmanapi.DeleteTradeTransInfo(t)

		return ret
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retError = err
	}

	return
}

func (m *DealerManager) ModifyTrade(trans *TradeTrans,
) (retOrder int, retError error) {

	taskFunc := func(api mtmanapi.CManagerInterface) int {
		total := 1
		order := trans.Order
		tr := api.TradeRecordsRequest(&order, &total)
		if total <= 0 {
			retError = mterr.InvalidTrade
			return mtmanapi.RET_OK
		}

		t := mtmanapi.NewTradeTransInfo()
		cmd := tr.GetCmd()

		t.SetPrice(tr.GetOpen_price())
		if cmd >= mtmanapi.OP_BUY_LIMIT && cmd <= mtmanapi.OP_SELL_STOP {
			t.SetPrice(trans.Price)
		}

		t.SetXtype(byte(mtmanapi.TT_BR_ORDER_MODIFY))
		t.SetOrderby(tr.GetLogin())
		t.SetOrder(tr.GetOrder())
		t.SetCmd(int16(cmd))
		t.SetSymbol(tr.GetSymbol())
		t.SetVolume(tr.GetVolume())
		t.SetTp(trans.Tp)
		t.SetSl(trans.Sl)

		ret := api.TradeTransaction(t)

		if ret != mtmanapi.RET_OK {
			retError = newMtErr(api, ret)
		} else {
			retOrder = t.GetOrder()
		}

		api.MemFree(tr.Swigcptr())
		mtmanapi.DeleteTradeTransInfo(t)

		return ret
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retError = err
	}

	return
}

// AdmModifyTrade ...
func (m *DealerManager) AdmModifyTrade(trade *Trade) (retError error) {
	taskFunc := func(api mtmanapi.CManagerInterface) int {
		var (
			total  = 1
			ticket = trade.Order
		)

		tr := api.TradeRecordsRequest(&ticket, &total)
		if total <= 0 {
			retError = mterr.InvalidTrade
			return mtmanapi.RET_OK
		}

		tr.SetSl(trade.Sl)
		tr.SetTp(trade.Tp)
		tr.SetProfit(trade.Profit)
		tr.SetCommission(trade.Commission)
		tr.SetTaxes(trade.Taxes)
		tr.SetStorage(trade.Swap)
		tr.SetOpen_price(trade.OpenPrice)
		tr.SetClose_price(trade.ClosePrice)
		tr.SetClose_time(trade.CloseTime)
		tr.SetOpen_time(trade.OpenTime)

		ret := api.AdmTradeRecordModify(tr)
		if ret != mtmanapi.RET_OK {
			retError = newMtErr(api, ret)
		}
		api.MemFree(tr.Swigcptr())
		return ret
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retError = err
	}
	return
}

func (m *DealerManager) GetTrade(ticket int) (retTrans *Trade, retError error) {

	taskFunc := func(api mtmanapi.CManagerInterface) int {
		total := 1
		order := ticket
		tr := api.TradeRecordsRequest(&order, &total)
		if total <= 0 {
			retError = mterr.InvalidTrade
			return mtmanapi.RET_OK
		}

		retTrans = &Trade{}
		retTrans.Login = tr.GetLogin()
		retTrans.Cmd = tr.GetCmd()
		retTrans.Symbol = tr.GetSymbol()
		retTrans.Volume = tr.GetVolume()
		retTrans.Order = tr.GetOrder()
		retTrans.OpenPrice = tr.GetOpen_price()
		retTrans.Comment = tr.GetComment()
		retTrans.Expiration = tr.GetExpiration()
		retTrans.OpenTime = tr.GetOpen_time()
		retTrans.ClosePrice = tr.GetClose_price()
		retTrans.CloseTime = tr.GetClose_time()
		retTrans.Commission = tr.GetCommission()
		retTrans.Swap = tr.GetStorage()
		retTrans.Profit = tr.GetProfit()
		retTrans.Tp = tr.GetTp()
		retTrans.Sl = tr.GetSl()

		api.MemFree(tr.Swigcptr())
		return mtmanapi.RET_OK
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retError = err
	}

	return
}

func (m *DealerManager) ServerTimezone() (tz int, retError error) {
	taskFunc := func(api mtmanapi.CManagerInterface) int {
		cfg := mtmanapi.NewConCommon()
		ret := api.CfgRequestCommon(cfg)
		if ret != mtmanapi.RET_OK {
			retError = newMtErr(api, ret)
			return ret
		}
		tz = cfg.GetTimezone()
		return mtmanapi.RET_OK
	}

	// wait as long as need?
	if err := newTask(m, taskFunc, 10*time.Second).run(); err != nil {
		retError = err
	}

	return
}

func (m *DealerManager) ServerTime() (stime int, retError error) {
	taskFunc := func(api mtmanapi.CManagerInterface) int {
		stime = api.ServerTime()
		return mtmanapi.RET_OK
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retError = err
	}

	return
}

func (m *DealerManager) ChartRequest(ci *ChartInfo) (ris []*RateInfo, timesign int, retError error) {
	taskFunc := func(api mtmanapi.CManagerInterface) int {
		c := mtmanapi.NewChartInfo()
		c.SetSymbol(ci.Symbol)
		c.SetPeriod(ci.Period)
		c.SetStart(ci.Start)
		c.SetEnd(ci.End)
		c.SetTimesign(ci.Timesign)
		c.SetMode(ci.Mode)

		timesign = 0
		total := 0
		rateinfos := api.ChartRequest(c, &timesign, &total)

		ris = make([]*RateInfo, 0, total)
		for i := 0; i < total; i++ {
			ri := mtmanapi.RateInfoArray_getitem(rateinfos, i)
			gori := &RateInfo{
				CTM:   ri.GetCtm(),
				Open:  ri.GetOpen(),
				High:  ri.GetHigh(),
				Low:   ri.GetLow(),
				Close: ri.GetClose(),
				Vol:   ri.GetVol(),
			}
			ris = append(ris, gori)
			defer mtmanapi.DeleteRateInfo(ri)
		}

		api.MemFree(rateinfos.Swigcptr())
		return mtmanapi.RET_OK
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retError = err
	}

	return
}

// TickInfoLast ...
func (m *DealerManager) TickInfoLast(symbol string) (tks []*TickInfo, retError error) {
	taskFunc := func(api mtmanapi.CManagerInterface) int {
		total := 0
		ticks := api.TickInfoLast(symbol, &total)

		tks = make([]*TickInfo, 0, total)
		for i := 0; i < total; i++ {
			tick := mtmanapi.TickInfoArray_getitem(ticks, i)
			gori := &TickInfo{
				Symbol: tick.GetSymbol(),
				Time:   tick.GetCtm(),
				Ask:    tick.GetAsk(),
				Bid:    tick.GetBid(),
			}
			tks = append(tks, gori)
			defer mtmanapi.DeleteTickInfo(tick)
		}

		api.MemFree(ticks.Swigcptr())
		return mtmanapi.RET_OK
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retError = err
	}

	return
}

func (m *DealerManager) UsersRequestByGroup(group string) (users []*User, retErr error) {
	taskFunc := func(api mtmanapi.CManagerInterface) int {
		var total int
		userRecords := api.AdmUsersRequest(group, &total)

		for i := 0; i < total; i++ {
			userRecord := mtmanapi.UserRecordArray_getitem(userRecords, i)
			users = append(users, newUser(userRecord))
			defer mtmanapi.DeleteUserRecord(userRecord)
		}

		api.MemFree(userRecords.Swigcptr())
		return mtmanapi.RET_OK
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retErr = err
	}
	return
}

// TradesRequestOpenOnly ...
func (m *DealerManager) TradesRequestOpenOnly(group string, login int) (trades []*Trade, retErr error) {
	taskFunc := func(api mtmanapi.CManagerInterface) int {
		var total int
		tradeRecords := api.AdmTradesRequest(group, 1, &total)

		for i := 0; i < total; i++ {
			tradeRecord := mtmanapi.TradeRecordArray_getitem(tradeRecords, i)

			if tradeRecord.GetLogin() == login || login == 0 {
				trades = append(trades, newTrade(tradeRecord))
			}
			defer mtmanapi.DeleteTradeRecord(tradeRecord)
		}

		api.MemFree(tradeRecords.Swigcptr())
		return mtmanapi.RET_OK
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retErr = err
	}
	return
}

func (m *DealerManager) TradesUserHistory(login, from, to int) (trades []*Trade, retErr error) {
	taskFunc := func(api mtmanapi.CManagerInterface) int {
		var total int
		tradeRecords := api.TradesUserHistory(login, from, to, &total)
		for i := 0; i < total; i++ {
			tradeRecord := mtmanapi.TradeRecordArray_getitem(tradeRecords, i)
			trades = append(trades, newTrade(tradeRecord))
			defer mtmanapi.DeleteTradeRecord(tradeRecord)
		}

		api.MemFree(tradeRecords.Swigcptr())
		return mtmanapi.RET_OK
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retErr = err
	}
	return
}

// AdminTradeRecordModify ...
// now only support change price and open/close time
func (m *DealerManager) AdminTradeRecordModify(trade *Trade) (retErr error) {
	taskFunc := func(api mtmanapi.CManagerInterface) int {
		total := 1
		tr := api.TradeRecordsRequest(&trade.Order, &total)
		if total <= 0 {
			retErr = mterr.InvalidTrade
			return mtmanapi.RET_OK
		}

		tr.SetClose_price(trade.ClosePrice)
		tr.SetOpen_price(trade.OpenPrice)
		tr.SetCommission(trade.Commission)

		if trade.OpenTime != 0 {
			tr.SetOpen_time(trade.OpenTime)
		}

		if trade.CloseTime != 0 {
			tr.SetClose_time(trade.CloseTime)
		}

		tr.SetProfit(trade.Profit)
		if trade.Swap != 0 {
			tr.SetStorage(trade.Swap)
		}

		api.AdmTradeRecordModify(tr)
		api.MemFree(tr.Swigcptr())
		return mtmanapi.RET_OK
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retErr = err
	}
	return
}

// GetHistoryTradeRecord ...
func (m *DealerManager) GetHistoryTradeRecord(order int) (trade *Trade, retErr error) {
	taskFunc := func(api mtmanapi.CManagerInterface) int {
		total := 1
		tr := api.TradeRecordsRequest(&order, &total)
		if total <= 0 {
			retErr = mterr.InvalidTrade
			return mtmanapi.RET_OK
		}

		trade = newTrade(tr)
		return mtmanapi.RET_OK
	}

	if err := newTask(m, taskFunc, time.Second).run(); err != nil {
		retErr = err
	}

	return
}

func (m *DealerManager) Timezone() int {
	return m.timezone
}
