package mtdealer

import (
	"git.what.codes/go/mtsdk/mtlog"
)

var nworker = 1

//var location *time.Location

var dealermgr *DealerManager

// DealerManager ...
type DealerManager struct {
	serverAddrs []string
	account     int
	password    string
	dealers     []*Dealer
	taskChan    chan *dealerTask
	started     bool
	workerNum   int
	timezone    int
	//	location    *time.Location
}

// NewDealerManager ...
func NewDealerManager(c *Config) *DealerManager {
	dealermgr = &DealerManager{
		serverAddrs: c.ServerAddrs,
		account:     c.Account,
		password:    c.Password,
		workerNum:   c.WorkerCount,
		timezone:    c.Timezone,
		taskChan:    make(chan *dealerTask, 1),
		started:     false,
	}
	return dealermgr
}

// Start ...
func (m *DealerManager) Start() {
	m.started = true
	for i := 0; i < m.workerNum; i++ {
		nworker++
		dealer := NewDealer(m, m.serverAddrs, m.account, m.password, nworker)
		m.dealers = append(m.dealers, dealer)
		go dealer.Run()
	}

	m.UpdateTimeZone()
}

// Stop ...
func (m *DealerManager) Stop() {
	m.started = false
	for _, dealer := range m.dealers {
		dealer.Stop()
	}
	m.dealers = make([]*Dealer, 0)
}

// IsStart ...
func (m *DealerManager) IsStart() bool {
	return m.started
}

// UpdateTimeZone ...
func (m *DealerManager) UpdateTimeZone() {
	tz, err := m.ServerTimezone()
	if err != nil {
		return
	}
	m.timezone = tz
	mtlog.Infof("UpdateTimeZone %d", tz)
	return
}
