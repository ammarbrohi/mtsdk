package mtdealer

import (
	"git.what.codes/go/mtsdk/mterr"
	"git.what.codes/go/mtsdk/mtmanapi"
	"sync"
	"time"
)

type TaskFunc func(mtmanapi.CManagerInterface) int

const (
	taskInit = iota
	taskRunning
	taskDone
	taskTimeout
)

type dealerTask struct {
	taskFunc TaskFunc
	manager  *DealerManager
	deadline time.Time
	status   int
	doneChan chan struct{}
	mu       sync.Mutex
}

func newTask(manager *DealerManager, taskFunc TaskFunc, timeout time.Duration,
) *dealerTask {
	return &dealerTask{
		manager:  manager,
		taskFunc: taskFunc,
		deadline: time.Now().Add(timeout),
		doneChan: make(chan struct{}),
		status:   taskInit,
	}
}

func (t *dealerTask) begin() int {
	t.mu.Lock()
	defer t.mu.Unlock()

	if t.status == taskInit {
		t.status = taskRunning
	}

	return t.status
}

func (t *dealerTask) timeout() int {
	t.mu.Lock()
	defer t.mu.Unlock()

	if t.status == taskInit {
		close(t.doneChan)
		t.status = taskTimeout
	}

	return t.status
}

func (t *dealerTask) done() int {
	t.mu.Lock()
	defer t.mu.Unlock()

	if t.status == taskRunning {
		close(t.doneChan)
		t.status = taskDone
	}

	return t.status
}

func (t *dealerTask) run() error {
	if !t.manager.started {
		return mterr.NewInternalError("Dealer not started")
	}

	select {
	case t.manager.taskChan <- t:
	case <-time.After(t.deadline.Sub(time.Now())):
		t.timeout()
		return mterr.NewServerBusy("task timeout before return")
	}

	select {
	case <-time.After(t.deadline.Sub(time.Now())):
		if t.timeout() == taskRunning {
			<-t.doneChan
		} else {
			return mterr.NewServerBusy("task timeout after return")
		}
	case <-t.doneChan:
	}

	return nil
}
