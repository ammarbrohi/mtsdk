package mtdealer

import (
	"errors"
	"fmt"
	"git.what.codes/go/mtsdk/mterr"
	"git.what.codes/go/mtsdk/mtlog"
	"git.what.codes/go/mtsdk/mtmanapi"
	"math"
	"time"
)

var factory mtmanapi.CManagerFactory
var apiVer = makelong(
	mtmanapi.ManAPIProgramBuild,
	mtmanapi.ManAPIProgramVersion,
)

func init() {
	factory = mtmanapi.NewCManagerFactory()
	factory.Init()

	if factory.IsValid() == 0 {
		panic("Failed to load mtmanapi dll.")
	}

	if factory.WinsockStartup() != mtmanapi.RET_OK {
		panic("WinsockStartup failed")
	}
}

func makelong(a, b int) int {
	return int(uint32(a) | uint32(b)<<16)
}

func manApiAuth(serverAddr string, account int, password string) (
	mtmanapi.CManagerInterface, error) {

	mtapi := factory.Create(apiVer)
	if mtapi == nil {
		return nil, fmt.Errorf("Failed to create manager interface.")
	}

	errno := mtapi.Connect(serverAddr)
	if errno != mtmanapi.RET_OK {
		desc := mtapi.ErrorDescription(errno)
		mtapi.Release()
		return nil, errors.New(desc)
	}

	errno = mtapi.Login(account, password)
	if errno != mtmanapi.RET_OK {
		desc := mtapi.ErrorDescription(errno)
		mtapi.Release()
		return nil, errors.New(desc)
	}

	return mtapi, nil
}

func manAPISelectAuth(serverAddrs []string, account int, password string) (
	mtmanapi.CManagerInterface, error) {

	mtapi := factory.Create(apiVer)
	if mtapi == nil {
		return nil, fmt.Errorf("Failed to create manager interface.")
	}

	selectAddr, err := selectAPIAddr(mtapi, serverAddrs)
	if err != nil {
		mtapi.Release()
		return nil, err
	}

	errno := mtapi.Connect(selectAddr)
	if errno != mtmanapi.RET_OK {
		desc := mtapi.ErrorDescription(errno)
		mtapi.Release()
		return nil, errors.New(desc)
	}

	errno = mtapi.Login(account, password)
	if errno != mtmanapi.RET_OK {
		desc := mtapi.ErrorDescription(errno)
		mtapi.Release()
		return nil, errors.New(desc)
	}

	return mtapi, nil
}

func selectAPIAddr(api mtmanapi.CManagerInterface, serverAddrs []string) (string, error) {
	var (
		minConTime = int64(math.MaxInt64)
		selectAddr string
	)

	for _, addr := range serverAddrs {
		start := time.Now()
		//start := time.Nanosecond()

		mtlog.Infof("Check Connect Addr %s", addr)
		errno := api.Connect(addr)
		if errno != mtmanapi.RET_OK {
			desc := api.ErrorDescription(errno)
			mtlog.Errorf("Addr %s connect Error[%s]", addr, desc)
			continue
		}
		elapsed := time.Since(start)
		//end := time.Nanosecond()
		passtime := int64(elapsed)
		mtlog.Infof("Connect Addr %s cost %+v min %d us", addr, elapsed, minConTime)
		if passtime < minConTime {
			minConTime = passtime
			selectAddr = addr
		}
	}

	if selectAddr == "" {
		return selectAddr, fmt.Errorf("Failed to create manager interface.")
	}
	mtlog.Infof("Connect Select %s cost %d us", selectAddr, minConTime)

	return selectAddr, nil
}

func GetMtErr(errno int) error {
	mtapi := factory.Create(apiVer)
	defer mtapi.Release()

	if mtapi == nil {
		return fmt.Errorf("Failed to create manager interface.")
	}

	var err error
	if errno == mtmanapi.RET_NO_CONNECT {
		err = mterr.NewServerBusy("Mt4 no connect.")
	} else {
		msg := mtapi.ErrorDescription(errno)
		err = mterr.NewMtError(errno, msg)
	}

	return err
}

func newMtErr(api mtmanapi.CManagerInterface, errno int) error {
	var err error
	if errno == mtmanapi.RET_NO_CONNECT {
		err = mterr.NewServerBusy("Mt4 no connect.")
	} else {
		msg := api.ErrorDescription(errno)
		err = mterr.NewMtError(errno, msg)
	}

	return err
}
