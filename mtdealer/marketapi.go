package mtdealer

import (
	"errors"
	"git.what.codes/go/mtsdk/mterr"
	"git.what.codes/go/mtsdk/mtmanapi"
	"math"
	"time"
)

func (m *MarketManager) SubQuote(quoteChan chan *Quote) int {
	m.dataMu.Lock()
	m.quoteId += 1
	quoteId := m.quoteId
	m.quoteReceivers[quoteId] = quoteChan
	m.dataMu.Unlock()

	return quoteId
}

func (m *MarketManager) UnsubQuote(quoteId int) {
	m.dataMu.Lock()
	delete(m.quoteReceivers, quoteId)
	m.dataMu.Unlock()
}

func (m *MarketManager) SubTradeNotify(f func(int, Trade)) {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()
	m.tradeReport = f
}

func (m *MarketManager) SubUserNotify(f func(int, User)) {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()
	m.userReport = f
}

func (m *MarketManager) SubGroupNotify(f func(int, Group)) {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()
	m.groupReport = f
}

func (m *MarketManager) SubSymbolNotify(f func(int, Symbol)) {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()
	m.symbolReport = f
}

func (m *MarketManager) SubMargin(login int) {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()

	_, ok := m.margins[login]
	if !ok {
		m.margins[login] = &MarginLevel{}
	}
}

func (m *MarketManager) SubOnPump(onPump chan int) {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()
	m.onPump = onPump
}

func (m *MarketManager) GetMargin(login int) *MarginLevel {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()
	return m.margins[login]
}

func (m *MarketManager) GetMtmanAPI() mtmanapi.CManagerInterface {
	return m.mtapi
}

func (m *MarketManager) GetTrades(filter func(*Trade) bool) []*Trade {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()

	return m.GetTradesLocked(filter)
}

func (m *MarketManager) GetTrade(order int) *Trade {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()

	return m.GetTradeLocked(order)
}

func (m *MarketManager) GetUsers(filter func(*User) bool) []*User {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()

	return m.GetUsersLocked(filter)
}

func (m *MarketManager) GetUsersByLogins(logins []int) []*User {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()

	var (
		findmap = make(map[int]bool)
		users   = make([]*User, 0)
	)
	for _, login := range logins {
		findmap[login] = true
	}

	for _, u := range m.users {
		if _, ok := findmap[u.Login]; ok {
			users = append(users, u)
		}
	}

	return users
}

func (m *MarketManager) GetSymbol(name string) *Symbol {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()

	return m.GetSymbolLocked(name)
}

func (m *MarketManager) GetGroup(name string) *Group {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()

	return m.GetGroupLocked(name)
}

func (m *MarketManager) GetUser(login int) *User {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()

	return m.GetUserLocked(login)
}

func (m *MarketManager) GetQuote(symbol string) *Quote {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()

	return m.GetQuoteLocked(symbol)
}

func (m *MarketManager) GetTradesLocked(filter func(*Trade) bool) []*Trade {
	trades := make([]*Trade, 0)
	for _, t := range m.trades {
		if filter == nil || filter(t) {
			trades = append(trades, t)
		}
	}
	return trades
}

func (m *MarketManager) GetTradeLocked(order int) *Trade {
	trade := m.trades[order]
	return trade
}

func (m *MarketManager) GetUsersLocked(filter func(*User) bool) []*User {
	users := make([]*User, 0)
	for _, u := range m.users {
		if filter == nil || filter(u) {
			users = append(users, u)
		}
	}
	return users
}

func (m *MarketManager) GetSymbolLocked(name string) *Symbol {
	s := m.symbols[name]
	return s
}

func (m *MarketManager) GetGroupLocked(name string) *Group {
	g := m.groups[name]
	return g
}

func (m *MarketManager) GetUserLocked(login int) *User {
	user := m.users[login]
	return user
}

func (m *MarketManager) Users() map[int]*User {
	return m.users
}

func (m *MarketManager) GetQuoteLocked(symbol string) *Quote {
	quote := m.quotes[symbol]
	return quote
}

func (m *MarketManager) IsTradable(login int, symbol string) bool {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()
	return m.IsTradableLocked(login, symbol)
}

func (m *MarketManager) IsTradableLocked(login int, symbol string) bool {
	s := m.GetSymbolLocked(symbol)
	u := m.GetUserLocked(login)

	if s == nil || u == nil {
		return false
	}

	g := m.GetGroupLocked(u.Group)
	sec := g.Secs[s.Type]

	if !sec.IsShow || !sec.IsTrade {
		return false
	}

	now := time.Now()
	_, offset := now.Zone()
	now = time.Now().Add(time.Duration(dealermgr.timezone)*time.Hour - time.Duration(offset)*time.Second)

	if !s.Tradable(&now) {
		return false
	}

	return true
}

func (m *MarketManager) GetPrice(login int, symbol string, cmd int) (float64,
	error) {

	m.dataMu.Lock()
	defer m.dataMu.Unlock()

	s := m.GetSymbolLocked(symbol)
	u := m.GetUserLocked(login)
	if s == nil || u == nil {
		return 0, errors.New("invalid user")
	}

	g := m.GetGroupLocked(u.Group)
	sec := g.Secs[s.Type]
	if !sec.IsShow || !sec.IsTrade {
		return 0, errors.New("untradable")
	}

	quote := m.GetQuoteLocked(symbol)
	if quote == nil {
		return 0, mterr.NewServerBusy("No price")
	}
	price := float64(0)

	pipes := math.Pow(0.1, float64(s.Digits))
	spread := Spread(cmd, sec.SpreadDiff) * pipes

	if cmd == mtmanapi.OP_BUY {
		price = quote.Ask + spread
	} else if cmd == mtmanapi.OP_SELL {
		price = quote.Bid - spread
	}

	return price, nil
}

// 在新用户和新交易刚完成时，此时Manager还没有收到推送，此时用户无法操作此数据
// 为了避免这种情况的发生，在新加入用户交易后，立刻调用此函数来加入数据
func (m *MarketManager) TryAddTrade(t *Trade) {
	m.dataMu.Lock()

	_, ok := m.trades[t.Order]
	if !ok {
		m.trades[t.Order] = t
	}

	m.dataMu.Unlock()
}

// CalTradeProfit ...
func (m *MarketManager) CalTradeProfit(t *Trade) float64 {
	return m.calTradeProfit(t)
}

func (m *MarketManager) TryAddUser(u *User) {
	m.dataMu.Lock()

	_, ok := m.users[u.Login]
	if !ok {
		m.users[u.Login] = u
	}

	m.dataMu.Unlock()
}

type GroupSymbol struct {
	// symbol
	Symbol           string
	Type             int
	Digits           int
	Currency         string
	MarginCurrency   string
	MarginInitial    float64
	MarginHedged     float64
	TickSize         float64
	TickValue        float64
	ContractSize     float64
	MarginMode       int
	ProfitMode       int
	StopsLevel       int
	Spread           int
	MarginPercentage float64
	Execution        int
	GTCPendings      int
	SwapType         int
	SwapLong         float64
	SwapShort        float64
	SwapDay          int
	Sessions         [7]Sessions

	// secgroup
	IsShow     bool
	IsTrade    bool
	LotMin     int
	LotMax     int
	LotStep    int
	SpreadDiff int
	// group
	Group string
}

func (m *MarketManager) GetGroupSymbolsLocked(group *Group) []*GroupSymbol {
	gss := make([]*GroupSymbol, 0, len(m.symbols))
	for _, symbol := range m.symbols {
		if len(group.Secs) <= symbol.Type {
			continue
		}

		sec := group.Secs[symbol.Type]
		if !sec.IsShow || !sec.IsTrade {
			continue
		}

		gs := &GroupSymbol{
			Symbol:           symbol.Symbol,
			Type:             symbol.Type,
			Digits:           symbol.Digits,
			Currency:         symbol.Currency,
			MarginCurrency:   symbol.MarginCurrency,
			MarginInitial:    symbol.MarginInitial,
			MarginHedged:     symbol.MarginHedged,
			TickSize:         symbol.TickSize,
			TickValue:        symbol.TickValue,
			ContractSize:     symbol.ContractSize,
			MarginMode:       symbol.MarginMode,
			ProfitMode:       symbol.ProfitMode,
			Sessions:         symbol.Sessions,
			StopsLevel:       symbol.StopsLevel,
			Spread:           symbol.Spread,
			MarginPercentage: symbol.MarginPercentage,
			Execution:        symbol.Execution,
			GTCPendings:      symbol.GTCPendings,
			SwapType:         symbol.SwapType,
			SwapLong:         symbol.SwapLong,
			SwapShort:        symbol.SwapShort,
			SwapDay:          symbol.SwapDay,

			IsShow:     sec.IsShow,
			IsTrade:    sec.IsTrade,
			LotMin:     sec.LotMin,
			LotMax:     sec.LotMax,
			LotStep:    sec.LotStep,
			SpreadDiff: sec.SpreadDiff,

			Group: group.Group,
		}
		gss = append(gss, gs)
	}
	return gss
}

func (m *MarketManager) GetAllGroupSymbols() []*GroupSymbol {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()

	gss := make([]*GroupSymbol, 0, len(m.symbols))
	for _, group := range m.groups {
		gss = append(gss, m.GetGroupSymbolsLocked(group)...)
	}

	return gss
}

// GetPlugin ...
func (m *MarketManager) GetPlugin(name string) *Plugin {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()

	plugin, ok := m.plugins[name]
	if !ok {
		return nil
	}
	return plugin
}

func (m *MarketManager) GetGroupSymbols(group *Group) []*GroupSymbol {
	m.dataMu.Lock()
	defer m.dataMu.Unlock()

	return m.GetGroupSymbolsLocked(group)
}

func (m *MarketManager) GetAsset(login int) (assest *Assest, retErr error) {
	user := m.GetUser(login)
	if user == nil {
		retErr = mterr.UserNotFound
		return
	}

	loginTrades := m.GetTrades(func(t *Trade) bool {
		return (t.Login == login && (t.Cmd == mtmanapi.OP_BUY || t.Cmd == mtmanapi.OP_SELL))
	})

	assest = &Assest{
		Login:      login,
		Leverage:   user.Leverage,
		Balance:    user.Balance,
		Credit:     user.Credit,
		FreeMargin: user.Balance + user.Credit,
		Equity:     user.Balance + user.Credit,
	}

	if len(loginTrades) == 0 {
		return
	}

	m.apiCall(func() {
		marginLevel := mtmanapi.NewMarginLevel()
		defer mtmanapi.DeleteMarginLevel(marginLevel)

		ret := m.mtapi.MarginLevelGet(login, user.Group, marginLevel)
		if ret != mtmanapi.RET_OK {
			retErr = newMtErr(m.mtapi, ret)
			return
		}

		assest.Margin = marginLevel.GetMargin()
		assest.FreeMargin = marginLevel.GetMargin_free()
		assest.MarginLevel = marginLevel.GetMargin_level()
		assest.Equity = marginLevel.GetEquity()
	})
	return
}

// GetUsersByGroup ..
func (m *MarketManager) GetUsersByGroup(group string) (users []*User) {
	filter := func(user *User) bool {
		if user.Group == group {
			return true
		}
		return false
	}
	return m.GetUsers(filter)
}
