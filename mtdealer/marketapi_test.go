package mtdealer

import (
	"log"
	"testing"
	"time"
)

const (
	tmReconnectTime = 5
)

var (
	tmconf = &Config{
		ServerAddrs: []string{"127.0.0.1:443"},
		Account:     1,
		Password:    "manager",
		WorkerCount: 1,
	}
	tmStopChan  = make(chan interface{})
	tmStartChan = make(chan interface{})
	tmFailChan  = make(chan interface{})
	marketMgr   = NewMarketManager(tmconf)
	dealerMgr   = NewDealerManager(tmconf)
)

func connecting() {
	var (
		ticker = time.Tick(2 * time.Second)
		times  = 0
	)
	marketMgr.Start()
	dealerMgr.Start()
	defer marketMgr.Stop()

	for {
		<-ticker
		if marketMgr.PumpStarted() {
			tmStartChan <- nil
			select {
			case <-tmStopChan:
				break
			}
		}
		times++
		log.Println("Connect Times : ", times)
		if times > tmReconnectTime {
			tmFailChan <- nil
			break
		}
	}
}

func TestConnect(t *testing.T) {
	go connecting()

	select {
	case <-tmStartChan:
		close(tmStopChan)
	case <-tmFailChan:
		t.Fatalf("Connect mt4 Timed out")
	}
}
