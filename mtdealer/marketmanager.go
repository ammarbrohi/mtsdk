package mtdealer

import (
	"git.what.codes/go/mtsdk/mtlog"
	"git.what.codes/go/mtsdk/mtmanapi"
	"runtime/debug"
	"sync"
	"time"
)

type Session struct {
	OpenHour  int `json:"open_hour"`
	OpenMin   int `json:"open_min"`
	CloseHour int `json:"close_hour"`
	CloseMin  int `json:"close_min"`
}

type Sessions struct {
	Quote [3]Session `json:"quote"`
	Trade [3]Session `json:"trade"`
}

type Symbol struct {
	Symbol           string
	Type             int
	Digits           int
	Currency         string
	MarginCurrency   string
	MarginInitial    float64
	MarginHedged     float64
	TickSize         float64
	TickValue        float64
	ContractSize     float64
	MarginMode       int
	ProfitMode       int
	StopsLevel       int
	Spread           int
	MarginPercentage float64
	Execution        int
	GTCPendings      int
	SwapType         int
	SwapLong         float64
	SwapShort        float64
	SwapDay          int
	Sessions         [7]Sessions
}

func (s *Symbol) Tradable(t *time.Time) bool {

	day := t.Weekday()
	session := s.Sessions[day]

	min := t.Hour()*60 + t.Minute()
	for _, q := range session.Quote {
		if q.OpenHour*60+q.OpenMin >= min {
			continue
		}

		if q.CloseHour*60+q.CloseMin <= min {
			continue
		}

		return true
	}

	return false
}

type SecGroup struct {
	IsShow     bool
	IsTrade    bool
	LotMin     int
	LotMax     int
	LotStep    int
	SpreadDiff int
}

type Group struct {
	Group           string
	Secs            []SecGroup
	DefaultLeverage int
	MarginCall      int
	MarginStopout   int
	Enable          int
}

type User struct {
	Login                int
	Name                 string
	Country              string
	City                 string
	Address              string
	Phone                string
	Email                string
	Group                string
	ID                   string // idCardNo
	Balance              float64
	Credit               float64
	Leverage             int
	Password             string
	PasswordInvestor     string
	Comment              string
	UserColor            uint
	AgentAccount         int
	Enable               int
	EnableReadOnly       int
	EnableChangePassword int
}

type Trade struct {
	Order      int     `json:"ticket"`
	Login      int     `json:"login"`
	Symbol     string  `json:"symbol"`
	Digits     int     `json:"digits"`
	Cmd        int     `json:"cmd"`
	Volume     int     `json:"volume"`
	OpenTime   int     `json:"open_time"`
	OpenPrice  float64 `json:"open_price"`
	CloseTime  int     `json:"close_time"`
	ClosePrice float64 `json:"close_price"`
	Sl         float64 `json:"sl"`
	Tp         float64 `json:"tp"`
	Profit     float64 `json:"profit"`
	Commission float64 `json:"commission"`
	Swap       float64 `json:"swap"`
	Comment    string  `json:"comment"`
	Expiration int     `json:"expiration"`
	Taxes      float64 `json:"-"`
	State      int
	GWVolume   int
}

type MarginLevel struct {
	Login       int
	Group       string
	Leverage    int
	Updated     int
	Balance     float64
	Equity      float64
	Volume      int
	Margin      float64
	MarginFree  float64
	MarginLevel float64
	MarginType  int
	LevelType   int
}

type Plugin struct {
	Pos     int
	Enabled int
	Params  map[string]string
}

type Quote struct {
	Symbol   string  `json:"symbol"`
	Ask      float64 `json:"ask"`
	Bid      float64 `json:"bid"`
	LastTime int     `json:"time"`
	Digits   int     `json:"-"`
}

type MarketManager struct {
	serverAddrs []string
	account     int
	password    string
	mtapi       mtmanapi.CManagerInterface
	receiver    mtmanapi.PumpReceiver

	users   map[int]*User
	trades  map[int]*Trade
	quotes  map[string]*Quote
	symbols map[string]*Symbol
	groups  map[string]*Group
	margins map[int]*MarginLevel
	plugins map[string]*Plugin

	stop        chan struct{}
	stopped     bool
	apiMu       sync.Mutex
	dataMu      sync.Mutex
	pumpStarted bool

	quoteId        int
	quoteReceivers map[int]chan *Quote

	onPump       chan int
	tradeReport  func(int, Trade)
	userReport   func(int, User)
	groupReport  func(int, Group)
	symbolReport func(int, Symbol)

	recontInternal int
}

func newMarginLevel(m mtmanapi.MarginLevel) *MarginLevel {
	return &MarginLevel{
		Login:       m.GetLogin(),
		Group:       m.GetGroup(),
		Leverage:    m.GetLeverage(),
		Updated:     m.GetUpdated(),
		Balance:     m.GetBalance(),
		Equity:      m.GetEquity(),
		Volume:      m.GetVolume(),
		Margin:      m.GetMargin(),
		MarginLevel: m.GetMargin_level(),
		MarginType:  m.GetMargin_type(),
		MarginFree:  m.GetMargin_free(),
		LevelType:   m.GetLevel_type(),
	}
}

func newUser(u mtmanapi.UserRecord) *User {
	return &User{
		Login:                u.GetLogin(),
		Name:                 u.GetName(),
		Country:              u.GetCountry(),
		City:                 u.GetCity(),
		Address:              u.GetAddress(),
		Phone:                u.GetPhone(),
		Email:                u.GetEmail(),
		Group:                u.GetGroup(),
		ID:                   u.GetId(),
		Balance:              u.GetBalance(),
		Credit:               u.GetCredit(),
		Leverage:             u.GetLeverage(),
		Password:             u.GetPassword(),
		PasswordInvestor:     u.GetPassword_investor(),
		Comment:              u.GetComment(),
		UserColor:            u.GetUser_color(),
		AgentAccount:         u.GetAgent_account(),
		Enable:               u.GetEnable(),
		EnableReadOnly:       u.GetEnable_read_only(),
		EnableChangePassword: u.GetEnable_change_password(),
	}
}

func newTrade(t mtmanapi.TradeRecord) *Trade {
	return &Trade{
		Order:      t.GetOrder(),
		Login:      t.GetLogin(),
		Symbol:     t.GetSymbol(),
		Digits:     t.GetDigits(),
		Cmd:        t.GetCmd(),
		Volume:     t.GetVolume(),
		OpenTime:   t.GetOpen_time(),
		OpenPrice:  t.GetOpen_price(),
		CloseTime:  t.GetClose_time(),
		ClosePrice: t.GetClose_price(),
		Sl:         t.GetSl(),
		Tp:         t.GetTp(),
		Comment:    t.GetComment(),
		GWVolume:   t.GetGw_volume(),
		State:      t.GetState(),
		Expiration: t.GetExpiration(),
		Profit:     t.GetProfit(),
		Swap:       t.GetStorage(),
		Commission: t.GetCommission(),
	}
}

func newSymbol(s mtmanapi.ConSymbol) *Symbol {
	return &Symbol{
		Symbol:           s.GetSymbol(),
		Type:             s.GetXtype(),
		Digits:           s.GetDigits(),
		Currency:         s.GetCurrency(),
		MarginCurrency:   s.GetMargin_currency(),
		MarginInitial:    s.GetMargin_initial(),
		MarginHedged:     s.GetMargin_hedged(),
		TickSize:         s.GetTick_size(),
		TickValue:        s.GetTick_value(),
		ContractSize:     s.GetContract_size(),
		MarginMode:       s.GetMargin_mode(),
		ProfitMode:       s.GetProfit_mode(),
		StopsLevel:       s.GetStops_level(),
		Spread:           s.GetSpread(),
		MarginPercentage: s.GetMargin_divider(),
		Execution:        s.GetExemode(),
		GTCPendings:      s.GetGtc_pendings(),
		SwapType:         s.GetSwap_type(),
		SwapLong:         s.GetSwap_long(),
		SwapShort:        s.GetSwap_short(),
		SwapDay:          s.GetSwap_rollover3days(),
	}
}

// NewMarketManager ..
func NewMarketManager(c *Config) *MarketManager {
	mgr := &MarketManager{
		serverAddrs:    c.ServerAddrs,
		account:        c.Account,
		password:       c.Password,
		stopped:        false,
		stop:           make(chan struct{}),
		quoteId:        0,
		recontInternal: 5,
		margins:        make(map[int]*MarginLevel),
		quoteReceivers: make(map[int]chan *Quote),
	}
	mgr.cleanup()
	return mgr
}

// Start ...
func (m *MarketManager) Start() {
	go m.run()
}

// Stop ...
func (m *MarketManager) Stop() {
	m.stopped = true
	close(m.stop)
}

// PumpStarted ..
func (m *MarketManager) PumpStarted() bool {
	return m.pumpStarted
}

func (m *MarketManager) run() {
	for !m.stopped {
		if m.mtapi == nil {
			mtlog.Infof("MarketManager connecting to %+v", m.serverAddrs)
			//mtapi, err := manAPIAuth(m.serverAddrs, m.account, m.password)
			mtapi, err := manAPISelectAuth(m.serverAddrs, m.account, m.password)

			if err != nil {
				mtlog.Infof("MarketManager connect error %s", err)
				select {
				case <-time.After(3 * time.Second):
				case <-m.stop:
				}

				time.Sleep(time.Duration(m.recontInternal) * time.Second)
				m.updateReconnectTime()
				continue
			}

			mtlog.Infof("MarketManager connectted")
			m.setApi(mtapi)
		}

		select {
		case <-time.After(5 * time.Second):
			if m.mtapi.IsConnected() == 0 {
				mtlog.Errorf("MarketManager disconnected")
				m.destroyApi()

				time.Sleep(time.Duration(m.recontInternal) * time.Second)
				m.updateReconnectTime()
			} else {
				m.recontInternal = 5
			}
		}
	}

	m.destroyApi()
}

func (m *MarketManager) updateReconnectTime() {
	m.recontInternal += 10
	if m.recontInternal > 120 {
		m.recontInternal = 5
	}
}

func (m *MarketManager) destroyApi() {
	m.apiMu.Lock()
	if m.mtapi != nil {
		m.mtapi.Release()
		mtmanapi.DeleteDirectorPumpReceiver(m.receiver)
		m.mtapi = nil
		m.receiver = nil
	}
	m.apiMu.Unlock()
}

func (m *MarketManager) setApi(mtapi mtmanapi.CManagerInterface) {
	m.cleanup()

	m.apiMu.Lock()
	m.mtapi = mtapi
	m.receiver = mtmanapi.NewDirectorPumpReceiver(m)
	flags := mtmanapi.CLIENT_FLAGS_HIDENEWS |
		mtmanapi.CLIENT_FLAGS_HIDEMAIL |
		mtmanapi.CLIENT_FLAGS_HIDEONLINE

	mtmanapi.PumpingSwitchEx(m.mtapi, flags, m.receiver)
	m.apiMu.Unlock()
}

func (m *MarketManager) apiCall(api func()) {
	m.apiMu.Lock()
	defer m.apiMu.Unlock()
	if m.mtapi == nil {
		return
	}
	api()
}

func (m *MarketManager) refreshUsers() {
	start := time.Now()

	m.apiCall(func() {
		var total int
		userRecords := m.mtapi.UsersGet(&total)
		if total != 0 && userRecords.Swigcptr() == 0 {
			panic("UsersGet return NULL")
		}

		userMap := make(map[int]*User, total)
		defer func() {
			m.users = userMap
			mtlog.Debugf("refreshUsers count:%d cost:%f", len(userMap), time.Now().Sub(start).Seconds())
		}()

		for i := 0; i < total; i++ {
			u := mtmanapi.UserRecordArray_getitem(userRecords, i)
			defer mtmanapi.DeleteUserRecord(u)

			user := newUser(u)
			userMap[user.Login] = user
		}

		m.mtapi.MemFree(userRecords.Swigcptr())

	})
}

func (m *MarketManager) refreshTrades() {
	start := time.Now()

	m.apiCall(func() {
		var total int
		tradeRecords := m.mtapi.TradesGet(&total)
		if total != 0 && tradeRecords.Swigcptr() == 0 {
			panic("TradesGet return NULL")
		}

		tradeMap := make(map[int]*Trade, total)
		defer func() {
			m.trades = tradeMap
			mtlog.Debugf("refreshTrades count:%d cost:%f", len(tradeMap), time.Now().Sub(start).Seconds())
		}()

		for i := 0; i < total; i++ {
			t := mtmanapi.TradeRecordArray_getitem(tradeRecords, i)
			defer mtmanapi.DeleteTradeRecord(t)

			trade := newTrade(t)
			tradeMap[trade.Order] = trade
		}

		m.mtapi.MemFree(tradeRecords.Swigcptr())
	})
}

func (m *MarketManager) refreshGroups() {
	start := time.Now()

	m.apiCall(func() {
		var total int
		conGroups := m.mtapi.GroupsGet(&total)
		if total != 0 && conGroups.Swigcptr() == 0 {
			panic("GroupsGet return NULL")
		}

		groupMap := make(map[string]*Group, total)
		defer func() {
			m.groups = groupMap
			mtlog.Debugf("refresGroups count:%d cost:%f", len(groupMap), time.Now().Sub(start).Seconds())
		}()

		for i := 0; i < total; i++ {
			g := mtmanapi.ConGroupArray_getitem(conGroups, i)
			defer mtmanapi.DeleteConGroup(g)

			group := &Group{
				Group:           g.GetGroup(),
				DefaultLeverage: g.GetDefault_leverage(),
				MarginCall:      g.GetMargin_call(),
				MarginStopout:   g.GetMargin_stopout(),
			}
			for i := 0; i < mtmanapi.MAX_SEC_GROUPS; i++ {
				sec := mtmanapi.ConGroupSecArray_getitem(g.GetSecgroups(), i)
				defer mtmanapi.DeleteConGroupSec(sec)

				group.Secs = append(group.Secs, SecGroup{
					IsShow:     sec.GetShow() != 0,
					IsTrade:    sec.GetTrade() != 0,
					LotMin:     sec.GetLot_min(),
					LotMax:     sec.GetLot_max(),
					LotStep:    sec.GetLot_step(),
					SpreadDiff: sec.GetSpread_diff(),
				})
			}

			groupMap[group.Group] = group
		}

		m.mtapi.MemFree(conGroups.Swigcptr())
	})
}

func (m *MarketManager) refreshSymbols() {
	start := time.Now()

	m.apiCall(func() {
		var total int
		conSymbols := m.mtapi.SymbolsGetAll(&total)
		if total != 0 && conSymbols.Swigcptr() == 0 {
			panic("SymbolsGetAll return NULL")
		}

		symbolMap := make(map[string]*Symbol, total)
		defer func() {
			m.symbols = symbolMap
			mtlog.Debugf("refreshSymbols count:%d cost:%f", len(symbolMap), time.Now().Sub(start).Seconds())
		}()

		syminfo := mtmanapi.New_SymbolInfoArray(1)
		defer mtmanapi.Delete_SymbolInfoArray(syminfo)

		for i := 0; i < total; i++ {
			s := mtmanapi.ConSymbolArray_getitem(conSymbols, i)
			defer mtmanapi.DeleteConSymbol(s)

			m.mtapi.SymbolAdd(s.GetSymbol())
			symbol := newSymbol(s)
			sessions := symbol.Sessions
			for j := 0; j < 7; j++ {
				mtsession := mtmanapi.ConSessionsArray_getitem(s.GetSessions(), j)
				defer mtmanapi.DeleteConSessions(mtsession)

				mtquote := mtsession.GetQuote()
				for k := 0; k < 3; k++ {
					t := mtmanapi.ConSessionArray_getitem(mtquote, k)
					defer mtmanapi.DeleteConSession(t)

					sessions[j].Quote[k].OpenHour = int(t.GetOpen_hour())
					sessions[j].Quote[k].OpenMin = int(t.GetOpen_min())
					sessions[j].Quote[k].CloseHour = int(t.GetClose_hour())
					sessions[j].Quote[k].CloseMin = int(t.GetClose_min())
				}

				mttrade := mtsession.GetTrade()
				for k := 0; k < 3; k++ {
					t := mtmanapi.ConSessionArray_getitem(mttrade, k)
					defer mtmanapi.DeleteConSession(t)

					sessions[j].Trade[k].OpenHour = int(t.GetOpen_hour())
					sessions[j].Trade[k].OpenMin = int(t.GetOpen_min())
					sessions[j].Trade[k].CloseHour = int(t.GetClose_hour())
					sessions[j].Trade[k].CloseMin = int(t.GetClose_min())
				}
			}

			symbol.Sessions = sessions
			symbolMap[symbol.Symbol] = symbol

			ret := m.mtapi.SymbolInfoGet(s.GetSymbol(), syminfo)
			if ret != mtmanapi.RET_OK {
				continue
			}
			quote := &Quote{
				Symbol:   s.GetSymbol(),
				Digits:   s.GetDigits(),
				Ask:      syminfo.GetAsk(),
				Bid:      syminfo.GetBid(),
				LastTime: syminfo.GetLasttime(),
			}

			m.dataMu.Lock()
			m.quotes[quote.Symbol] = quote
			m.dataMu.Unlock()
		}

		m.mtapi.MemFree(conSymbols.Swigcptr())
	})
}

func (m *MarketManager) updateQuotes() {
	m.apiCall(func() {
		symbolInfos := mtmanapi.New_SymbolInfoArray(32)
		defer mtmanapi.Delete_SymbolInfoArray(symbolInfos)
		total := m.mtapi.SymbolInfoUpdated(symbolInfos, 32)

		for i := 0; i < total; i++ {
			q := mtmanapi.SymbolInfoArray_getitem(symbolInfos, i)
			defer mtmanapi.DeleteSymbolInfo(q)

			quote := &Quote{
				Symbol:   q.GetSymbol(),
				Digits:   q.GetDigits(),
				Ask:      q.GetAsk(),
				Bid:      q.GetBid(),
				LastTime: q.GetLasttime(),
			}

			m.dataMu.Lock()
			m.quotes[quote.Symbol] = quote
			m.dataMu.Unlock()

			go m.feedQuote(quote)
		}
	})

}

func (m *MarketManager) cleanup() {
	m.dataMu.Lock()
	m.users = make(map[int]*User)
	m.trades = make(map[int]*Trade)
	m.quotes = make(map[string]*Quote)
	m.symbols = make(map[string]*Symbol)
	m.groups = make(map[string]*Group)
	m.plugins = make(map[string]*Plugin)
	m.dataMu.Unlock()
}

func (m *MarketManager) updateTrade(typ int, data uintptr) {
	if data == 0 {
		return
	}

	t := mtmanapi.TradeRecord(mtmanapi.SwigcptrTradeRecord(data))
	trade := newTrade(t)

	m.dataMu.Lock()
	switch typ {
	case mtmanapi.TRANS_ADD:
		m.trades[trade.Order] = trade
	case mtmanapi.TRANS_DELETE:
		delete(m.trades, trade.Order)
	case mtmanapi.TRANS_UPDATE:
		if trade.Login == 0 {
			delete(m.trades, trade.Order)
		} else {
			m.trades[trade.Order] = trade
		}
	default:
	}
	m.dataMu.Unlock()

	m.updateUserByLogin(trade.Login)
	if m.tradeReport != nil {
		m.tradeReport(typ, *trade)
	}

	mtlog.Debugf("UpdateTrades: %d", len(m.trades))

}

func (m *MarketManager) getUser(login int) (user *User, retErr error) {
	m.apiCall(func() {
		u := mtmanapi.NewUserRecord()
		defer mtmanapi.DeleteUserRecord(u)

		ret := m.mtapi.UserRecordGet(login, u)
		if ret != mtmanapi.RET_OK {
			retErr = newMtErr(m.mtapi, ret)
		}
		user = newUser(u)
	})

	return
}

// updateUserByLogin
func (m *MarketManager) updateUserByLogin(login int) {
	user, err := m.getUser(login)
	if err != nil {
		mtlog.Errorf("Refresh user:%d error:%s after trade update.",
			login, err)
		return
	}

	m.dataMu.Lock()
	defer m.dataMu.Unlock()

	m.users[user.Login] = user
}

// UserUpdateType
const (
	UpdateUserEnable = iota
	UpdatePassword
	UpdateGroup
	UpdateLeverage
)

// manager update not effect
func (m *MarketManager) updateUser(typ int, data uintptr) {
	if data == 0 {
		return
	}
	u := mtmanapi.UserRecord(mtmanapi.SwigcptrUserRecord(data))
	user := newUser(u)

	if preuser, ok := m.users[user.Login]; ok {
		// disable user
		chgtype := 0
		if user.Enable == 0 {
			chgtype = 1 << UpdateUserEnable
		}

		if preuser.Password != user.Password {
			chgtype = 1<<UpdatePassword + chgtype
		}

		// update group
		if preuser.Group != user.Group {
			chgtype = 1<<UpdateGroup + chgtype
		}

		// update leverage
		if preuser.Leverage != user.Leverage {
			chgtype = 1<<UpdateLeverage + chgtype
		}

		if m.userReport != nil && chgtype > 0 {
			m.userReport(chgtype, *user)
		}
	} else if m.userReport != nil {
		m.userReport(-1, *user)
	}

	m.dataMu.Lock()
	switch typ {
	case mtmanapi.TRANS_DELETE:
		delete(m.users, user.Login)
	default:
		m.users[user.Login] = user
	}
	m.dataMu.Unlock()

	mtlog.Debugf("UpdateUsers: %d type:%d", len(m.users), typ)
}

// GroupUpdateType
const (
	UpdateGroupEnable = iota
	UpdateSecurity
)

func (m *MarketManager) updateGroup(typ int, data uintptr) {
	if data == 0 {
		return
	}

	g := mtmanapi.ConGroup(mtmanapi.SwigcptrConGroup(data))
	group := &Group{
		Group:           g.GetGroup(),
		DefaultLeverage: g.GetDefault_leverage(),
		MarginCall:      g.GetMargin_call(),
		MarginStopout:   g.GetMargin_stopout(),
		Enable:          g.GetEnable(),
	}

	for i := 0; i < mtmanapi.MAX_SEC_GROUPS; i++ {
		sec := mtmanapi.ConGroupSecArray_getitem(g.GetSecgroups(), i)
		defer mtmanapi.DeleteConGroupSec(sec)

		group.Secs = append(group.Secs, SecGroup{
			IsShow:     sec.GetShow() != 0,
			IsTrade:    sec.GetTrade() != 0,
			LotMax:     sec.GetLot_max(),
			LotMin:     sec.GetLot_min(),
			LotStep:    sec.GetLot_step(),
			SpreadDiff: sec.GetSpread_diff(),
		})
	}

	// check group
	if pregroup, ok := m.groups[group.Group]; ok {
		// check enable
		chgtype := 0
		if group.Enable == 0 {
			chgtype = 1 << UpdateGroupEnable
		}

		// check security
		if len(pregroup.Secs) != len(group.Secs) {
			chgtype = 1<<UpdateSecurity + chgtype
		} else {
			for i := 0; i < len(group.Secs); i++ {
				preSec := pregroup.Secs[i]
				sec := group.Secs[i]
				if preSec.IsTrade != sec.IsTrade ||
					preSec.IsShow != sec.IsShow ||
					preSec.LotMax != sec.LotMax ||
					preSec.LotMin != sec.LotMin ||
					preSec.LotStep != sec.LotStep ||
					preSec.SpreadDiff != sec.SpreadDiff {

					chgtype = 1<<UpdateSecurity + chgtype
					break
				}
			}
		}

		if m.groupReport != nil {
			m.groupReport(chgtype, *group)
		}
	}

	m.dataMu.Lock()
	m.groups[group.Group] = group
	m.dataMu.Unlock()

	mtlog.Debugf("UpdateGroups: %d", len(m.groups))
}

func (m *MarketManager) updateMargin() {
	m.apiCall(func() {
		for login := range m.margins {
			if user, ok := m.users[login]; ok {
				marginLevel := mtmanapi.NewMarginLevel()
				defer mtmanapi.DeleteMarginLevel(marginLevel)
				ret := m.mtapi.MarginLevelGet(login, user.Group, marginLevel)
				if ret != mtmanapi.RET_OK {
					continue
				}
				m.margins[login] = newMarginLevel(marginLevel)
			}
		}
	})
}

func (m *MarketManager) updateSymbol(typ int, data uintptr) {
	if data == 0 {
		return
	}

	s := mtmanapi.ConSymbol(mtmanapi.SwigcptrConSymbol(data))
	symbol := newSymbol(s)

	if m.symbolReport != nil {
		m.symbolReport(typ, *symbol)
	}

	/*
		m.dataMu.Lock()
		m.symbols[symbol.Symbol] = symbol
		m.dataMu.Unlock()

		go m.feedSymbol(symbol)
	*/
}

func (m *MarketManager) refreshPlugins() {
	mtlog.Info("refresh Plugin")
	m.apiCall(func() {
		var total int
		plugins := m.mtapi.PluginsGet(&total)
		if total != 0 && plugins.Swigcptr() == 0 {
			mtlog.Error("refresh Plugin")
			return
		}

		pluginmap := make(map[string]*Plugin, total)
		for i := 0; i < total; i++ {
			plugin := mtmanapi.ConPluginArray_getitem(plugins, i)
			defer mtmanapi.DeleteConPlugin(plugin)
			mtlog.Info("refresh Plugin", plugin.GetFile())

			param := mtmanapi.NewConPluginParam()
			defer mtmanapi.DeleteConPluginParam(param)

			ret := m.mtapi.PluginParamGet(i, param)
			if ret != mtmanapi.RET_OK {
				mtlog.Errorf("Get Plugin Param Error %d plugin %d", ret, i)
				continue
			}

			mp := &Plugin{
				Pos:     i,
				Enabled: plugin.GetEnabled(),
				Params:  make(map[string]string, param.GetTotal()),
			}

			for j := 0; j < param.GetTotal(); j++ {
				p := mtmanapi.PluginCfgArray_getitem(param.GetParams(), j)
				defer mtmanapi.DeletePluginCfg(p)
				mp.Params[p.GetName()] = p.GetValue()
			}
			pluginmap[plugin.GetFile()] = mp
		}
		m.mtapi.MemFree(plugins.Swigcptr())

		m.dataMu.Lock()
		m.plugins = pluginmap
		m.dataMu.Unlock()
	})
}

func (m *MarketManager) OnPump(code, typ int, data uintptr) {
	defer func() {
		if e := recover(); e != nil {
			mtlog.Errorf("MarketManager OnPump:%s stack:\n%s", e, debug.Stack())
		}
	}()

	switch code {
	case mtmanapi.PUMP_START_PUMPING:
		mtlog.Info("MarketManager OnPump Start")
		m.refreshGroups()
		m.refreshSymbols()
		m.refreshTrades()
		m.refreshUsers()
		m.updateQuotes()
		dealermgr.UpdateTimeZone()
		m.pumpStarted = true
	case mtmanapi.PUMP_UPDATE_PLUGINS:
		m.refreshPlugins()
	case mtmanapi.PUMP_UPDATE_SYMBOLS:
		m.refreshSymbols()
		m.updateSymbol(typ, data)
	case mtmanapi.PUMP_UPDATE_BIDASK:
		m.updateQuotes()
		m.updateMargin()
	case mtmanapi.PUMP_UPDATE_GROUPS:
		m.updateGroup(typ, data)
	case mtmanapi.PUMP_UPDATE_USERS:
		m.updateUser(typ, data)
	case mtmanapi.PUMP_UPDATE_TRADES:
		m.updateTrade(typ, data)
	case mtmanapi.PUMP_STOP_PUMPING:
		m.pumpStarted = false
	default:
	}

	if m.onPump != nil {
		m.onPump <- code
	}
}

func (m *MarketManager) feedQuote(quote *Quote) {
	for _, quoteChan := range m.quoteReceivers {
		select {
		case quoteChan <- quote:
		default:
		}
	}
}

func (m *MarketManager) calTradeProfit(trade *Trade) float64 {
	var profit float64
	m.apiCall(func() {
		tr := mtmanapi.NewTradeRecord()
		tr.SetLogin(trade.Login)
		tr.SetVolume(trade.Volume)
		tr.SetCmd(trade.Cmd)
		tr.SetSymbol(trade.Symbol)
		tr.SetOpen_price(trade.OpenPrice)
		tr.SetClose_price(trade.ClosePrice)

		m.mtapi.TradeCalcProfit(tr)
		profit = tr.GetProfit()
	})
	return profit
}
