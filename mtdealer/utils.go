package mtdealer

import (
	"git.what.codes/go/mtsdk/mtmanapi"
	"math"
	"regexp"
	"strings"
)

func Spread(cmd int, diff int) float64 {

	if diff > 0 {
		if cmd == mtmanapi.OP_BUY {
			return math.Ceil(float64(diff) / 2)
		}
		if cmd == mtmanapi.OP_SELL {
			return float64(diff / 2)
		}
	}

	if diff < 0 {
		if cmd == mtmanapi.OP_BUY {
			return float64(diff / 2)
		}
		if cmd == mtmanapi.OP_SELL {
			return math.Floor(float64(diff) / 2)
		}
	}

	return 0
}

// CheckBid ...
func CheckBid(value, bid int) bool {
	if (uint(value) & (1 << uint(bid))) > 0 {
		return true
	}
	return false
}

// Match ...
func Match(expression, s string) bool {
	var (
		pattrens = strings.Split(expression, ",")
		include  = false
	)

	for i := 0; i < len(pattrens); i++ {
		pattren := strings.Replace(pattrens[i], ".", "\\.", -1)
		pl := len(pattren)
		if pl == 0 {
			continue
		}

		fc := pattren[0]
		isnot := false
		if fc == '!' {
			isnot = true
			pattren = pattren[1:]
		}

		if !isnot && include {
			continue
		}

		pl = len(pattren)
		if pl == 0 {
			continue
		}

		if pattren[0] == '*' {
			pattren = "." + pattren
		} else {
			pattren = "^" + pattren
		}

		if pattren != ".*" {
			if pattren[pl] == '*' {
				pattren = string(pattren[:pl]) + ".*"
			} else {
				pattren = pattren + "$"
			}
		}

		reg, err := regexp.Compile(pattren)
		if err != nil {
			continue
		}

		include = reg.MatchString(s)
		if include && isnot {
			return false
		}
	}

	return include
}
