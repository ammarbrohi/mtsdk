package mtenv

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
)

var (
	DIR         string
	RUN_DIR     string
	LOG_DIR     string
	LOG_PATH    string
	CONFIG_PATH string
	CONFIG_DIR  string
	PID         int
	SIGNAL      int
	HOSTNAME    string
	CMD         string
)

const (
	STOP  = 1
	START = 2
)

func init() {

	file, _ := filepath.Abs(os.Args[0])
	dir := filepath.Dir(file)

	DIR = filepath.Dir(dir + "..")

	LOG_DIR = DIR + "/logs/"
	LOG_PATH = LOG_DIR + filepath.Base(os.Args[0]) + ".log"
	CONFIG_DIR = DIR + "/conf/"
	CONFIG_PATH = CONFIG_DIR + filepath.Base(os.Args[0]) + ".conf"
	RUN_DIR, _ := os.Getwd()

	flag.StringVar(&CONFIG_PATH, "c", CONFIG_PATH, "")

	var signal string
	flag.StringVar(&signal, "s", signal, "")
	flag.StringVar(&CMD, "cmd", "", "")

	switch signal {
	case "stop":
		SIGNAL = STOP
		break
	case "start":
		SIGNAL = START
		break
	}

	if !filepath.IsAbs(CONFIG_PATH) {
		CONFIG_PATH = DIR + "/" + CONFIG_PATH
	}

	RUN_DIR, _ = filepath.Abs(RUN_DIR)
	CONFIG_PATH, _ = filepath.Abs(CONFIG_PATH)
	CONFIG_DIR, _ = filepath.Abs(CONFIG_DIR)
	LOG_DIR, _ = filepath.Abs(LOG_DIR)
	LOG_PATH, _ = filepath.Abs(LOG_PATH)

	PID = os.Getpid()

	hostname, err := os.Hostname()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	HOSTNAME = hostname
}

func AbsPath(path string) string {

	if !filepath.IsAbs(path) {
		path = DIR + "/" + path
	}

	path, _ = filepath.Abs(path)

	return path
}
