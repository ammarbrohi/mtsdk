package mterr

import "fmt"

type BllError struct {
	msg string
}

func NewBllError(format string, a ...interface{}) *BllError {
	return &BllError{
		msg: fmt.Sprintf(format, a...),
	}
}

func (e *BllError) Error() string {
	return e.msg
}

type MtError struct {
	Msg   string
	Errno int
}

func NewMtError(errno int, msg string) *MtError {
	return &MtError{
		Msg:   msg,
		Errno: errno,
	}
}

func (e *MtError) Error() string {
	return e.Msg
}

type ServerBusy struct {
	msg string
}

func (e *ServerBusy) Error() string {
	return e.msg
}

func NewServerBusy(msg string) *ServerBusy {
	return &ServerBusy{
		msg: msg,
	}
}

type ServerError struct {
	msg    string
	detail string
}

func (e *ServerError) Error() string {
	return e.msg
}

func (e *ServerError) Detail() string {
	return e.detail
}

func NewServerError(msg string, detailFmt string, a ...interface{}) *ServerError {
	return &ServerError{
		msg:    msg,
		detail: fmt.Sprintf(detailFmt, a...),
	}
}

func NewInternalError(detailFmt string, a ...interface{}) *ServerError {
	return NewServerError("Internal error", detailFmt, a...)
}

var (
	// Bll err
	FunctionNotFound  = NewServerError("Function not found", "Function not found")
	PageSizeTooLarge  = NewBllError("Page size too large(larger than 1000).")
	JsonDecodeError   = NewBllError("Request decode JSON error")
	UserNotFound      = NewBllError("User not found")
	InvalidCmd        = NewBllError("Invalid cmd")
	InvalidSymbol     = NewBllError("Invalid symbol")
	InvalidTrade      = NewBllError("Invalid trade")
	NoPermission      = NewBllError("No permission")
	InvalidGroup      = NewBllError("Invalid group")
	IncorrectPassword = NewBllError("Incorrect password")
	UserAlreadyExists = NewBllError("User Already Exists")
	UserExisted       = NewBllError("User Already Exists")
	UserNotExist      = NewBllError("User Not Found")
	InvalidPrice      = NewBllError("Invalid Price")
	InvalidLogin      = NewBllError("InvalidLogin")
	SystemError       = NewBllError("System Error")

	// Tc api Err
	StillFollowed      = NewBllError("Trader is still followed")
	IsFollowing        = NewBllError("Client is following")
	TraderNotFound     = NewBllError("Trader not found")
	TraderCannotFollow = NewBllError("Trader cannot follow")
	InvalidParameter   = NewBllError("Invalid parameter")
)
