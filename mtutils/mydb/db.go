package reportdb

import (
	"database/sql"
	"fmt"
	"reflect"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	goqu "gopkg.in/doug-martin/goqu.v3"
	_ "gopkg.in/doug-martin/goqu.v3/adapters/mysql"
)

type Db struct {
	*sql.DB
	Sqlx *sqlx.DB
	Goqu *goqu.Database
}

type Filter struct {
	goqu.Ex
	filterMap map[string]interface{}
}

func ArgsFilter(filterMap map[string]interface{}) *Filter {
	return &Filter{
		Ex:        goqu.Ex{},
		filterMap: filterMap,
	}
}

func (f *Filter) Where(field string, op string, filterField string) *Filter {
	value, ok := f.filterMap[filterField]
	if ok {
		switch op {
		case "eq", "neq":
			kind := reflect.TypeOf(value).Kind()
			switch kind {
			case reflect.Int, reflect.Int16, reflect.Int32, reflect.Int64,
				reflect.Float32, reflect.Float64, reflect.String:
				f.Ex[field] = goqu.Op{op: value}
			default:
			}
		case "between":
			values, ok := value.([]interface{})
			if ok && len(values) == 2 {
				f.Ex[field] = goqu.Op{
					"between": goqu.RangeVal{
						Start: values[0], End: values[1],
					},
				}
			}
		default:
		}

	}
	return f
}

func (f *Filter) End() []goqu.Expression {
	var ex []goqu.Expression
	if len(f.Ex) > 0 {
		ex = append(ex, f.Ex)
	}
	return ex
}

func NewDb(uri string) (*Db, error) {
	db, err := sql.Open("mysql", uri)
	if err != nil {
		return nil, err
	}

	goquDb := goqu.New("mysql", db)
	return &Db{
		DB:   db,
		Goqu: goquDb,
	}, nil
}

func (db *Db) PageQuery(sqlBuilder *goqu.Dataset, pageIndex int,
	pageSize int, outRows interface{}, selectEx ...interface{},
) (int, error) {
	selectBuilder := sqlBuilder.
		Offset(uint(pageIndex * pageSize)).
		Limit(uint(pageSize))

	for _, ex := range selectEx {
		selectBuilder = selectBuilder.Select(ex)
	}

	err := selectBuilder.ScanStructs(outRows)
	if err != nil {
		return 0, err
	}

	count, err := sqlBuilder.Count()
	if err != nil {
		return 0, err
	}

	return int(count), nil
}

func (db *Db) DebugSql(sqlBuilder *goqu.Dataset) {
	sql, args, err := sqlBuilder.ToSql()
	fmt.Println("Sql", sql, args, err)
}
