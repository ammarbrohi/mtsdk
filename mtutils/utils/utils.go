package utils

import (
	"git.what.codes/go/mtsdk/mtlog"
	"os"
	"os/signal"
	"syscall"
)

func exit(sigc os.Signal) {
	mtlog.Warnf("receive signal:%s exiting", sigc)
	os.Exit(0)
}

func HandleSignal() {

	sigc := make(chan os.Signal)
	signal.Notify(sigc, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGHUP,
		syscall.SIGTERM)

	for {

		s := <-sigc

		switch s {
		case syscall.SIGINT:
			exit(s)
		case syscall.SIGQUIT:
			exit(s)
		case syscall.SIGTERM:
			exit(s)
		case syscall.SIGHUP:

		default:

		}
	}

}
